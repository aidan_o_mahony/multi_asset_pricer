#!/bin/bash
# Purpose: Read Comma Separated CSV File
# Author: Vivek Gite under GPL v2.0+
# ------------------------------------------
INPUT=opt_pricer_batch.config
OLDIFS=$IFS
IFS=','
i=0
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read european american call num_layers num_dim remainder
do
        sed -i "s/OPT_EUROPEAN.*$/OPT_EUROPEAN $european/g" opt_pricer.config 
        sed -i "s/OPT_AMERICAN.*$/OPT_AMERICAN $american/g" opt_pricer.config 
        sed -i "s/CALL_PUT.*$/CALL_PUT $call/g" opt_pricer.config 
        sed -i "s/NUM_LAYERS.*$/NUM_LAYERS $num_layers/g" opt_pricer.config 
        sed -i "s/NUM_DIM.*$/NUM_DIM $num_dim/g" opt_pricer.config 
	#echo "European : $european"
	#echo "American : $american"
	#echo "Call/put : $call"
	#echo "Num layers : $num_layers"
	#echo "Dimensions  : $num_dim"
	#echo "Remainders : $remainder"
	remainder_array=()
	for str in ${remainder// / } ; do 
		remainder_array+=( $str )
        done
	i=0
	z=$((num_dim-1))
	y=$((num_dim*num_dim))
	c=$((y-1))
	d=$((num_dim+1))
	init_share=""
		IFS='/'
	for (( j=0; j<$num_dim; j++ ))
	do
		init_share+="${remainder_array[i]}"
		if [ $j -lt $z ]
		then
                init_share+=","
	        fi
		i=$((i+1))
	done
	#echo $init_share
        sed -i "s/INIT_SHARE_VALUE.*$/INIT_SHARE_VALUE $init_share/g" opt_pricer.config 
	sigma=""
	for (( j=0; j<$y; j++ ))
	do
		sigma+="${remainder_array[i]}"
		if [ $j -lt $c ]
		then
                sigma+=","
	        fi
		i=$((i+1))
	done
#	echo $sigma 
        sed -i "s/SIGMA.*$/SIGMA $sigma/g" opt_pricer.config 
	prob=""
	for (( j=0; j<$d; j++ ))
	do
		prob+="${remainder_array[i]}"
		if [ $j -lt $num_dim ]
		then
                prob+=","
	        fi
		i=$((i+1))
	done
#	echo $prob
        sed -i "s/PROB.*$/PROB $prob/g" opt_pricer.config 
	T="${remainder_array[i]}"
	i=$((i+1))
#	echo $T
        sed -i "s/^T.*$/T $T/g" opt_pricer.config 
	RATE="${remainder_array[i]}"
	i=$((i+1))
#	echo $RATE
        sed -i "s/RATE.*$/RATE $RATE/g" opt_pricer.config 
	strike=""
	for (( j=0; j<$num_dim; j++ ))
	do
		strike+="${remainder_array[i]}"
		if [ $j -lt $z ]
		then
                strike+=","
	        fi
		i=$((i+1))
	done
#	echo $strike
        sed -i "s/STRIKE.*$/STRIKE $strike/g" opt_pricer.config 
	MAX_MIN="${remainder_array[i]}"
	i=$((i+1))
#	echo $MAX_MIN
        sed -i "s/MAX_MIN.*$/MAX_MIN $MAX_MIN/g" opt_pricer.config 
	CASH="${remainder_array[i]}"
	i=$((i+1))
#	echo $CASH
        sed -i "s/CASH.*$/CASH $CASH/g" opt_pricer.config 
	VERBOSE="${remainder_array[i]}"
	i=$((i+1))
#	echo $VERBOSE
        sed -i "s/VERBOSE.*$/VERBOSE $VERBOSE/g" opt_pricer.config 

	ROTATION_ANGLE="${remainder_array[i]}"
	i=$((i+1))
        sed -i "s/ROTATION_ANGLE.*$/ROTATION_ANGLE $ROTATION_ANGLE/g" opt_pricer.config 


        IFS=','


       cd bin
       timeout 15 ./host ../opt_pricer.config 
       cd ..
done < $INPUT
IFS=$OLDIFS
