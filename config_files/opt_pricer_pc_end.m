asset1=100; %40 
asset2=40; %40 
sigma1=0.2;%1/5;
sigma2=0.5;%3/10;
corr=0.5;
r=0;%0.04879; % neutral risk rate
T=0.5833333333;%7/12;% orizzont
K=45;%35;%strike
call_put=1;%1 call, 0 pull
max_min=1;%1 MAX, 0 MIN
N=100; %timesteps
cash =0;
k=2;
for i=1:k
    for j=1:(k+1)
        if i==j
            M(i,j)= sqrt((k-i+1)/(k-i+2));
        else
            if i<j<k+2
                M(i,j)=-1/sqrt((k-i+1)*(k-i+2));
            end
            if j<i
                M(i,j)=0;
            end
        end
    end
end
E=[sigma1^2 corr*sigma1*sigma2; corr*sigma1*sigma2 sigma2^2] %covariance matrix

L=chol(E); %decompiosition Cholesky of covariance matrix
L=L';
A=L*M; %useful for calculate direction
dt=T/N;
R=exp(r*dt);
% hat{mu}
for i=1:k
    mu(i)=r-0.5*E(i,i);
end 
%the direction
for j=1:k %asset 
    for i=1:(k+1) %direction
        b=sqrt((k+1)*dt);
        d(j,i)=exp(b*A(j,i)+mu(j)*dt);
    end
end

% we can calculate the risk neutral measeure
q=[d; R*ones(1,1+k)]\[R*ones(1+k,1)];

%Prendo le direzioni in forma di colonna
d1=d(:,1);
d2=d(:,2);
d3=d(:,3);

%Chiamo B la matrice di covarianza trovata inizialmente ( la sigma old)
B=(q(1)*d1*d1'+q(2)*d2*d2'+q(3)*d3*d3')-R^2;
%in Matlab R^2 me lo sottrae per ogni elemento, altrimenti va moltiplicato per la matrice (1 1 ; 1 1).

%E=covariance_matrix*dt;
%Enew=E*dt; %old way

Enew(1,1)=exp(2*r*dt)*(exp(sigma1^2*dt)-1);
Enew(2,2)=exp(2*r*dt)*(exp(sigma2^2*dt)-1);
Enew(2,1)=exp(2*r*dt)*(exp(sigma1*sigma2*corr*dt)-1);
Enew(1,2)=exp(2*r*dt)*(exp(sigma1*sigma2*corr*dt)-1);

L1=chol(B)';
L2=chol(Enew)';

Lnew=L2*inv(L1);

% k è la dimensione, il numero dei sottostanti;
%new direction
dnew=Lnew*(d-R*ones(k,k+1))+R*ones(k,k+1)
%new risk probability measure
qnew=[dnew; R*ones(1,1+k)]\[R*ones(1+k,1)];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% ANAL SOLUTION CALCULATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sigmastar=sigma1^2-2*corr*sigma1*sigma2+sigma2^2;

%calculate Cmax with K

d1_1=(log(asset1/K)+(r+sigma1^2*0.5)*T)/(sigma1*sqrt(T));
ds_12=(log(asset1/asset2)+(sigmastar/2)*T)/(sqrt(sigmastar*T));
x=[d1_1; ds_12];
rho1=(sigma1-corr*sigma2)/sqrt(sigmastar);
SIGMA=[1 rho1;rho1 1];
term1=asset1*mvncdf(x,[0;0],SIGMA);

d1_2=(log(asset2/K)+(r+sigma2^2*0.5)*T)/(sigma2*sqrt(T));
ds_21=(log(asset2/asset1)+(sigmastar/2)*T)/(sqrt(sigmastar*T));
x=[d1_2; ds_21];
rho2=(sigma2-corr*sigma1)/sqrt(sigmastar);
SIGMA=[1 rho2;rho2 1];
term2=asset2*mvncdf(x,[0;0],SIGMA);

d2_1=(log(asset1/K)+(r-sigma1^2*0.5)*T)/(sigma1*sqrt(T));
d2_2=(log(asset2/K)+(r-sigma2^2*0.5)*T)/(sigma2*sqrt(T));
x=[-d2_1; -d2_2];
SIGMA=[1 corr;corr 1];
term3=K*exp(-r*T)*(1-mvncdf(x,[0;0],SIGMA));

callmaxK=term1+term2-term3
%calculate Cmin with K

x=[d1_1; -ds_12];
SIGMA=[1 -rho1;-rho1 1];
term1=asset1*mvncdf(x,[0;0],SIGMA);

x=[d1_2; -ds_21];
SIGMA=[1 -rho2;-rho2 1];
term2=asset2*mvncdf(x,[0;0],SIGMA);

x=[d2_1; d2_2];
SIGMA=[1 corr;corr 1];
term3=K*exp(-r*T)*mvncdf(x,[0;0],SIGMA);

callminK=term1+term2-term3

K0=0;

%calculate Cmax with 0

d1_1=(log(asset1/K0)+(r+sigma1^2*0.5)*T)/(sigma1*sqrt(T));
ds_12=(log(asset1/asset2)+(sigmastar/2)*T)/(sqrt(sigmastar*T));
x=[d1_1; ds_12];
rho1=(sigma1-corr*sigma2)/sqrt(sigmastar);
SIGMA=[1 rho1;rho1 1];
term1=asset1*mvncdf(x,[0;0],SIGMA);

d1_2=(log(asset2/K0)+(r+sigma2^2*0.5)*T)/(sigma2*sqrt(T));
ds_21=(log(asset2/asset1)+(sigmastar/2)*T)/(sqrt(sigmastar*T));
x=[d1_2; ds_21];
rho2=(sigma2-corr*sigma1)/sqrt(sigmastar);
SIGMA=[1 rho2;rho2 1];
term2=asset2*mvncdf(x,[0;0],SIGMA);

d2_1=(log(asset1/K0)+(r-sigma1^2*0.5)*T)/(sigma1*sqrt(T));
d2_2=(log(asset2/K0)+(r-sigma2^2*0.5)*T)/(sigma2*sqrt(T));
x=[-d2_1; -d2_2];
SIGMA=[1 corr;corr 1];
term3=K0*exp(-r*T)*(1-mvncdf(x,[0;0],SIGMA));

callmax0=term1+term2-term3;

%calculate Cmin with 0

x=[d1_1; -ds_12];
SIGMA=[1 -rho1;-rho1 1];
term1=asset1*mvncdf(x,[0;0],SIGMA);

x=[d1_2; -ds_21];
SIGMA=[1 -rho2;-rho2 1];
term2=asset2*mvncdf(x,[0;0],SIGMA);

x=[d2_1; d2_2];
SIGMA=[1 corr;corr 1];
term3=K0*exp(-r*T)*mvncdf(x,[0;0],SIGMA);

callmin0=term1+term2-term3;

%put
pminK=K*exp(-r*T)-callmin0+callminK
pmaxK=K*exp(-r*T)-callmax0+callmaxK

%print only the usefull one
if call_put == 1  %call
  if max_min==1  %call on max
    ANALYTICAL_SOL=callmaxK;
  else %call on min
    ANALYTICAL_SOL=callminK;
  end;
else
  if max_min==1  %put on max        
    ANALYTICAL_SOL=pmaxK;
  else %put on min
    ANALYTICAL_SOL=pminK;
  end;
end ;
sprintf('      ANALYTICL_SOL=%.20f',ANALYTICAL_SOL)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% VISUALIZA PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

num_dim=2;
  bin_string_MAX_MIN=sprintf('%d',mod(max_min+1,2));
  bin_string_NUM_LAYERS=dec2bin(N,15);
  hex_string_MAX_MNI_NUM_LAYERS=dec2hex(bin2dec([bin_string_MAX_MIN,bin_string_NUM_LAYERS]),8);
  
  hex_string_INIT_SHARE=cell(1,num_dim); 

  hex_string_INIT_SHARE{1}=num2hex(asset1*(dnew(1,1)^N));
  hex_string_INIT_SHARE{2}=num2hex(asset2*(dnew(2,1)^N));

  hex_string_STRIKE=cell(1,num_dim); 
  hex_string_STRIKE{1}=num2hex(K);
  hex_string_STRIKE{2}=num2hex(K);

  hex_string_CASH=num2hex(cash);

  hex_string_INIT_STEP=cell(num_dim,num_dim);
  for i=num_dim:-1:1
    for  j=num_dim:-1:1
      aa=dnew(i,j+1)/dnew(i,j);
      hex_string_INIT_STEP{i,j}=num2hex(aa);
    end
  end

  hex_string_INIT_STEP_NEXT=cell(1,num_dim);  
  for j=num_dim:-1:1
    aa=1/dnew(j,1);
    hex_string_INIT_STEP_NEXT{j}=num2hex(aa);
  end
  hex_string_MULT_COEFF=cell(1,num_dim+1);   
  for j=num_dim+1:-1:1
    aa=(1/R)*qnew(j);
    hex_string_MULT_COEFF{j}=num2hex(aa);  
  end
  hex_string_SHARE_COEFF=cell(1,num_dim); 
  for j=num_dim:-1:1
    aa=1/dnew(j,1);
    hex_string_SHARE_COEFF{j}=num2hex(aa);
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% Print Test Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sprintf(' INIT_SHARE{1} (hex) = %s',hex_string_INIT_SHARE{1})
sprintf(' INIT_SHARE{2} (hex) = %s',hex_string_INIT_SHARE{2})
sprintf(' MULT_COEFF{1} (hex) = %s',hex_string_MULT_COEFF{1})
sprintf(' MULT_COEFF{2} (hex) = %s',hex_string_MULT_COEFF{2})
sprintf(' MULT_COEFF{3} (hex) = %s',hex_string_MULT_COEFF{3})
sprintf('     STRIKE{1} (hex) = %s',hex_string_STRIKE{1})
sprintf('     STRIKE{2} (hex) = %s',hex_string_STRIKE{2})
sprintf('       MAX_MIN (bin) = %s',bin_string_MAX_MIN)
sprintf('          CASH (hex) = %s',hex_string_CASH)
sprintf('    NUM_LAYERS (bin) = %s',bin_string_NUM_LAYERS)
sprintf('SHARE_COEFF{1} (hex) = %s',hex_string_SHARE_COEFF{1})
sprintf('SHARE_COEFF{2} (hex) = %s',hex_string_SHARE_COEFF{2})
sprintf('  INIT_STEP{1} (hex) = %s',hex_string_INIT_STEP{1})
sprintf('  INIT_STEP{2} (hex) = %s',hex_string_INIT_STEP{2})
sprintf('  INIT_STEP{3} (hex) = %s',hex_string_INIT_STEP{3})
sprintf('  INIT_STEP{4} (hex) = %s',hex_string_INIT_STEP{4})
sprintf('  INIT_STEP_NEXT{1} (hex) = %s',hex_string_INIT_STEP_NEXT{1})
sprintf('  INIT_STEP_NEXT{2} (hex) = %s',hex_string_INIT_STEP_NEXT{2})
