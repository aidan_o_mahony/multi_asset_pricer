#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((num_compute_units(2,1)))
__attribute__((autorun))

kernel void dataMux  (  )
{	
	int  processingUnit = get_compute_id(0);
	type_payout     x;
	type_payout_NWS __attribute__((register)) y;
	 
	while (1) {
		#pragma ii 1
		for (char j = 0; j < PAYOUT_N; ++j) {
			x = read_channel_intel(writePayoutBufferChannel[processingUnit]);
			y.payoutN.payoutN[j] = x;
			y.status = 0;
			//printf("mux: %f, tree Status: %x, pre: %x, Finished: %d\n",x.payout, x.treeStatus, x.treePreSortStatus, y.status);
			if ( ((x.treeStatus >> TREE_END_LINE_BIT) & 0x0001) == 0x0001) {
				y.status = 1;
				break;
			}
		}
		write_channel_intel(writePayoutBufferChannelMux[processingUnit],y);
	}	
}		

