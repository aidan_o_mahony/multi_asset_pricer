#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable

kernel void channelDDR0  ( __global type_payout * restrict payoutBuffer )
{	
	unsigned int   writePointer = 0;
	unsigned int   readPointer  = 0;
	char finished = 0;
	bool status = 0;
	bool finalStatus = 0;
	type_payout x;
	type_payout y;
  int ddrChannel = 0;

	while (1) {
		bool channelStatus  = read_channel_intel(writePayoutBufferFinishChannel[ddrChannel]);
		if (channelStatus) break;
		writePointer = 0;
		finished = 0;
		#pragma ii 1
		#pragma ivdep
		while (finished == 0) {
			x = read_channel_intel(writePayoutBufferChannel[ddrChannel]);
			y = x;
			//printf("write Pointer %d,  finished %d\n",writePointer, finished);
			payoutBuffer[writePointer] = y;
			++writePointer;
			if ( ((x.treeStatus >> TREE_END_LINE_BIT) & 0x0001) == 0x0001) {
				finished = 1; 
			}		
		}		
		mem_fence(CLK_GLOBAL_MEM_FENCE);
		readPointer = 0;
		#pragma ii 1
		#pragma ivdep
		while (writePointer != 0) {
			--writePointer;
			type_payout z = payoutBuffer[readPointer];
			write_channel_intel(readPayoutBufferChannel[ddrChannel],z);
			//printf("Read Pointer %d, Write Pointer: %d\n",readPointer, writePointer);
			++readPointer;
		}
	}	
}		

