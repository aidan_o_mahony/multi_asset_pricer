#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((num_compute_units(2,1)))
__attribute__((autorun))

kernel void dataDeMux  (  )
{	
	int  processingUnit = get_compute_id(0);
	type_payout   x;
	type_payout_N __attribute__((register)) y;
	 
	while (1) {
		y = read_channel_intel(readPayoutBufferChannelDeMux[processingUnit]);
		#pragma ii 1
		for (char j = 0; j < PAYOUT_N; ++j) {
			x = y.payoutN[j];
			//printf ("j: %d, v=%f, s=%d, pres=%d\n",j,x.payout,x.treeStatus,x.treePreSortStatus);
			write_channel_intel(readPayoutBufferChannel[processingUnit],x);
			if ( ((x.treeStatus >> TREE_END_LINE_BIT) & 0x0001) == 0x0001) {
				break;
			}
		}
	}	
}		

