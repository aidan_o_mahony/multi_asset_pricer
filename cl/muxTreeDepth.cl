#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
//__attribute__((max_global_work_dim(0)))
//__attribute__((autorun))
kernel void muxTreeDepth ( )
{	

	bool control = 0;
	bool finished = 0;
	unsigned int treeDepth;

	while (finished == 0) {
		if (control == 0) {
			treeDepth = (unsigned int) read_channel_intel(muxTreeDepthChannel);
			control   = 1;
		}
		else {
			treeDepth = read_channel_intel(currentTreeDepthChannel[COMPUTE_DEPTH]);
		}
		write_channel_intel(currentTreeDepthChannel[0], treeDepth);
		if ((treeDepth & TREE_DEPTH_MASK) == 0) {
			finished = 1;
			break;
		}
	}
	//printf("......tree mux exit........................\n");
}
