def main():
    """ Code that takes change as input
    and returns the change
    """
    import numpy as np
    tree_depth = int(input("Tree Depth: "))

    num_assets = 7
    num_assets_p1 = num_assets + 1
    fifo_depth = np.array([2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=np.int32)
    max_fifo_depth = np.array([2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=np.int32)
    wc = np.array([2,0, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=np.int32)
    newp = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=np.int32)
    done = 0
    old = 2
    first = 1
    numPoints = 0
    while (done == 0):
        #if (old == 128):
            #print (wc)
        #print (wc)
        numPoints = numPoints +1
        endp = wc[num_assets]
        wc[num_assets] = 0
        endp_p1 = endp + 1
        if (endp == tree_depth):
        #if (endp == 3):
            done=1;
        j = num_assets -1
        if (newp[num_assets] == 1):
            wc[num_assets] = 1
            newp[num_assets]=0
        stop = 0
        if (first == 1):
            #wc[0] = 2
            wc[1] = 1 
            first = 0
        else:
            for i in range(num_assets):
                if (endp == 0 and wc[j] == 1 and stop == 0):
                    wc[j] = 0
                    wc[j+1] = 1
                    stop = 1
                if (endp == 0 and wc[j] == 0 and newp[j] == 1 and stop == 0):
                        wc[j] += 1
                        newp[j] = 0
                        stop = 1
                if (endp != 0 and wc[j] == endp_p1 and j == 0 and stop == 0):
                    wc[j+1]   = wc[j]
                    wc[j]   += 1
                    newp[j+2] = 1
                    stop = 1
                if (endp != 0 and wc[j] == endp_p1 and j != 0 and stop == 0):	
                    wc[j+1]   = endp_p1
                    wc[j]     = 0
                    newp[j+2] = 1
                    stop = 1
                if (endp != 0 and wc[j] >= endp_p1 and j != 0 and stop == 0):
                    wc[j+1]   = endp_p1
                    newp[j+2] = 1
                    stop = 1
                j -= 1 
            for i in range(num_assets):
                if (wc[i] != 0 and wc[i] != (tree_depth+1)):
                    fifo_depth[i] +=1
                    max_fifo_depth[i] = max(fifo_depth[i],max_fifo_depth[i])
            if (wc[num_assets] != 0):
                for i in range(num_assets):
                    fifo_depth[i] -=1
            if (old != wc[0]):
            	print (old, max_fifo_depth, numPoints)
            	old = wc[0]

														
								 
if __name__== "__main__":
    main()
