#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))

kernel void readMultiTierBuffer ( )
{	
  type_payout x, y;

	char finished = 0;
	char activeReadBuffer  = 3;
        char status0;
        char status1;
        char status;
	while (finished == 0) {
		if ( (activeReadBuffer &0x3) == 3) {
			activeReadBuffer = read_channel_intel(nextBuff);
		}
		if ((activeReadBuffer & 0x3) == 0) {
			y = read_channel_intel(readPayoutBufferChannel[0]);
		}
		if ( (activeReadBuffer & 0x3) == 1) {
			y = read_channel_intel(readPayoutBufferChannel[1]);
		}
		if ( (activeReadBuffer & 0x03) == 2) {
			y = read_channel_intel(multiBufferChannel2);
			status0 = (char) ((y.treeStatus >> TREE_START_LINE_BIT) & 0x0001);
			status1 = (char) ((y.treeStatus >> TREE_END_LINE_BIT)   & 0x0001);
			status  = status0 & status1;
			
		  if (status == 0x01) {
		  	finished = 1;
		  }
		}	
		write_channel_intel(payoutChannelReturn, y);

		status1 = (char) ((y.treeStatus >> TREE_END_LINE_BIT)   & 0x0001);
		if ( ((activeReadBuffer & 0x03) != 2) && (status1 == 0x1) ) {
			activeReadBuffer = 3;
		}

	}
}
