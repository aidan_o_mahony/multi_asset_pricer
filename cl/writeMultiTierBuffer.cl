#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))

kernel void writeMultiTierBuffer (  )
{	
  type_payout x, y;

	bool endLine = 0;
	bool startLine = 0;
	
	
	unsigned short treeDepth = INT_BUFF_MAX_TREE_DEPTH;
	bool finished = 0;
	unsigned char activeWriteBuffer = 0;
	bool status;
	bool doneSwitching = 0;

	char activeReadBuffer  = 0;
	while (finished== 0) {
		endLine = 0;
		if (activeWriteBuffer == 0)	{write_channel_intel(writePayoutBufferFinishChannel[0], 0);}
		if (activeWriteBuffer == 1)	{write_channel_intel(writePayoutBufferFinishChannel[1], 0);}

	  #pragma ii 1
		while (endLine == 0) {
			x = read_channel_intel(euPricerOutChannel[COMPUTE_DEPTH]);
			endLine = (x.treeStatus >> TREE_END_LINE_BIT)   & 0x0001;
			if ((activeWriteBuffer & WRITE_POINTER_MASK) == 0) {
				write_channel_intel(writePayoutBufferChannel[0], x);	
			} else if ((activeWriteBuffer & WRITE_POINTER_MASK) == 1) {
				write_channel_intel(writePayoutBufferChannel[1], x);	
		  	} else  {
				write_channel_intel(multiBufferChannel2, x);
			}
		}
		startLine   = (x.treeStatus >> TREE_START_LINE_BIT) & 0x0001;
		treeDepth   =  (x.treeStatus) & TREE_DEPTH_MASK;
		if ((activeWriteBuffer & WRITE_POINTER_MASK) == 0) {
			activeReadBuffer  = 0;
			activeWriteBuffer = 1;
			//printf ("EndLIne ARB: %d, %d\n",  activeReadBuffer, activeWriteBuffer);  
		} else if ((activeWriteBuffer & WRITE_POINTER_MASK) == 1) {
			activeReadBuffer  = 1;
			activeWriteBuffer = 0;
			//printf ("EndLIne ARB: %d,  %d\n",  activeReadBuffer, activeWriteBuffer);  
		} else {
			activeReadBuffer = 2;
			//printf ("EndLIne AWB: %d, TreeDepth: %d, %d, %d\n",  activeReadBuffer, endLine, startLine, treeDepth);  
		}  	
		if (treeDepth < INT_BUFF_MAX_TREE_DEPTH) {
			activeWriteBuffer = 2;
		} 
		//printf ("EndLIne AWB: %d, %d, : %d, %d\n",  activeWriteBuffer, endLine, startLine, treeDepth);  
		if (doneSwitching == 0) {
			//printf ("WA: %d\n", activeReadBuffer);
			write_channel_intel(nextBuff, activeReadBuffer);
			if ( (activeReadBuffer & 0x03) == 2) {
				doneSwitching = 1;					
				write_channel_intel(writePayoutBufferFinishChannel[0], 1);
				write_channel_intel(writePayoutBufferFinishChannel[1], 1);
			}
			//printf ("WB: %d\n", doneSwitching);
		}
		//if ((treeDepth & TREE_DEPTH_MASK) == 0 && endLine == 1) {
		if (startLine == 1 && endLine == 1) {
			finished = 1;
		}
	/* End of Writting to Internal/External Buffers */
	}	
			//printf ("EXIT %d\n", doneSwitching);
}		

