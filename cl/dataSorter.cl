#include "include/optPricer.h"


#pragma OPENCL EXTENSION cl_intel_channels : enable
__attribute__((max_global_work_dim(0)))
__attribute__((num_compute_units(COMPUTE_DEPTH,1)))
__attribute__((autorun))
kernel void dataSorter ( )
{	

	int  processingUnit = get_compute_id(0);

	type_payout payout;
	bool finished = 0;
	bool last = 0;
	bool lastw = 0;
  bool done = 0;
  bool first = 0;
	unsigned short depth;
	unsigned short i,j;
	unsigned short depthChannel;
	unsigned short currentTreeDepth;
	unsigned short newTreeDepth;
  unsigned int   currentTreeStatus;
  unsigned int   newTreeStatus;
  
	done  = 0;
	first = 1;
	//printf ("Read %d Tree Status\n", processingUnit); 
	currentTreeStatus = read_channel_intel(currentTreeDepthChannel[processingUnit]);
	//printf ("Finished Read %d Tree Status\n", processingUnit); 
	currentTreeDepth  =  ((unsigned short)currentTreeStatus) & TREE_DEPTH_MASK;
	newTreeStatus = currentTreeStatus & (~TREE_DEPTH_MASK);
	newTreeDepth  = 0;
	depth  = (~currentTreeDepth & TREE_DEPTH_MASK) + 2 ;
	//printf ("Node: %d --------------------Current Tree Status ----- %x \n" ,processingUnit, currentTreeStatus); 
	if (currentTreeDepth == 1) {
		last = 1;
		newTreeStatus |=  (processingUnit << LAST_OFFSET);
	} else if (currentTreeDepth == 0) {
		finished = 1;
		if ((currentTreeStatus >> LAST_OFFSET) == processingUnit) {
			last = 1;
		}
	} else {
		newTreeDepth = currentTreeDepth - 1;
		finished = 0;
		last = 0;
		newTreeStatus = 0;
	}
	newTreeStatus |= (newTreeDepth & TREE_DEPTH_MASK);
	//printf ("New Tree Status ----- %x  \n" ,newTreeStatus); 
	if ((last != 1) || (finished != 1)) {
		write_channel_intel(currentTreeDepthChannel[processingUnit+ 1], newTreeStatus);	
	}


  lastw = last;	
	while (done == 0) {
		payout = read_channel_intel(euPricerOutChannel[processingUnit]);
		if ((last == 1) && (finished == 1)) break;
		payout.treeStatus = (payout.treeStatus & ~TREE_DEPTH_MASK) | ( newTreeDepth & TREE_DEPTH_MASK);
		//Setting payout LAST bit status (payout.last = last);
		if (last == 0) {
			payout.treeStatus     &= (~(1 << TREE_LAST_BIT));
		} else {
			payout.treeStatus     |=   (1 << TREE_LAST_BIT);
		}
		//Setting payout FINISHED bit status (payout.finished = finished);
		if (finished == 0) {
			payout.treeStatus     &= (~(1 << TREE_FINISHED_BIT));
		} else {
			payout.treeStatus     |=   (1 << TREE_FINISHED_BIT);
		}
		//printf ("PUNIT: %d, DEPTH = %d, pre = %04x, Status = %04x,  Current tree = %d, Last = %d, Finished = %d  \n", processingUnit, depth, payout.treePreSortStatus, payout.treeStatus, currentTreeDepth, last, finished);

	  // FIRST three will always Happen	Minimum Assets = 2		
		//if (( (payout.treePreSortStatus & 1) != 0) || lastw == 1 || finished == 1){
		if ((((payout.treePreSortStatus & 1) != 0) && ( (((depth >> END_DEPTH_COUNTER_BIT) & ~depth)&0x0001) != 1)) || lastw == 1 || finished == 1){
			write_channel_intel(euPricerInChannel_0[processingUnit], payout);
			lastw = 0;
		}
		
		if (((payout.treePreSortStatus >> 1) & 0x0001) != 0 || finished == 1){
			write_channel_intel(euPricerInChannel_1[processingUnit], payout);
		}

		if (((payout.treePreSortStatus >> 2) & 0x0001) != 0 || finished == 1){
			write_channel_intel(euPricerInChannel_2[processingUnit], payout);
		}

		// These will happen IF NUM_ASSETS >=3
		if (NUM_ASSETS > 2) {
			if (((payout.treePreSortStatus >> 3) & 0x0001) != 0 || finished == 1){
				write_channel_intel(euPricerInChannel_3[processingUnit], payout);
			}
		}

		// These will happen IF NUM_ASSETS >=4
		if (NUM_ASSETS > 3) {
			if (((payout.treePreSortStatus >> 4) & 0x0001) != 0 || finished == 1){
				write_channel_intel(euPricerInChannel_4[processingUnit], payout);
			}
    }
		// These will happen IF NUM_ASSETS >=5
		if (NUM_ASSETS > 4) {
			if (((payout.treePreSortStatus >> 5) & 0x0001) != 0 || finished == 1){
				write_channel_intel(euPricerInChannel_5[processingUnit], payout);
			}
		}

		// These will happen IF NUM_ASSETS >=6
		if (NUM_ASSETS > 5) {
			if (((payout.treePreSortStatus >> 6) & 0x0001) != 0 || finished == 1){
				write_channel_intel(euPricerInChannel_6[processingUnit], payout);
			}
		}

		// These will happen IF NUM_ASSETS >=7
		if (NUM_ASSETS > 6) {
			if (((payout.treePreSortStatus >> 7) & 0x0001) != 0 || finished == 1){
				write_channel_intel(euPricerInChannel_7[processingUnit], payout);
			}
		}

		// These will happen IF NUM_ASSETS >=8
		if (NUM_ASSETS > 7) {
			if (((payout.treePreSortStatus >> 8) & 0x0001) != 0 || finished == 1){
				write_channel_intel(euPricerInChannel_8[processingUnit], payout);
			}
		}

		// These will happen IF NUM_ASSETS >=9
		if (NUM_ASSETS > 8) {
			if (((payout.treePreSortStatus >> 9) & 0x0001) != 0 || finished == 1){
				write_channel_intel(euPricerInChannel_9[processingUnit], payout);
			}
		}
		//printf ("  UNIT: %d,  depth: %x, done = %d, last = %d,  finished = %d, depth = %d, treeStatus = %x, presort = %x\n", processingUnit, depth, done, last, finished, depth, payout.treeStatus, payout.treePreSortStatus);
		if (((payout.treePreSortStatus >> TREE_PRESORT_END_LINE_BIT) & 0x0001) == 0x1) {
		  ++depth;
		}
		bool breakOut = (payout.treeStatus >> TREE_END_LINE_BIT) & 0x001;
		if ( breakOut ) {
			break;
		} 		
	}
	//printf ("Data Sorter Exit # Node= %d... Finished= %d, Last=%d, Current Tree Depth= %d\n", processingUnit, finished, last, currentTreeDepth);
}
