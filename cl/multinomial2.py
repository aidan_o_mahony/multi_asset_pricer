def main():
    """ Code that takes change as input
    and returns the change
    """
    import numpy as np
    tree_depth = int(input("Tree Depth: "))
    " num_assets MAXIMUM SUPPORTED = 7 "
    num_assets     = 5
    num_assets_p1  = num_assets + 1
    wc             = np.array([1, 0, 0, 0, 0, 0, 0, 0],    dtype=np.int32)
    newp           = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=np.int32)
    done      = 0
    old       = 2
    first     = 1
    val       = 0xFF
    endLine   = 1
    startLine = 1
    while (done == 0):
        print (wc, format(val, '08b'), startLine, endLine)
        startLine = 0
        endLine = 0
        val = 0
        endp = wc[num_assets]
        wc[num_assets] = 0
        endp_p1 = endp + 1
        if (endp == tree_depth):
            done=1;
        j = num_assets -1
        if (newp[num_assets] == 1):
            wc[num_assets] = 1
            newp[num_assets]=0
        stop = 0
        if (first == 1):
            wc[0] = 2 
            wc[1] = 1 
            first = 0
            startLine = 1
        else:
            for i in range(num_assets):
                if (endp == 0 and wc[j] == 1 and stop == 0):
                    wc[j] = 0
                    wc[j+1] = 1
                    stop = 1
                if (endp == 0 and wc[j] == 0 and newp[j] == 1 and stop == 0):
                        wc[j] += 1
                        newp[j] = 0
                        stop = 1
                if (endp != 0 and wc[j] == endp_p1 and j == 0 and stop == 0):
                    wc[j+1]   = wc[j]
                    wc[j]   += 1
                    newp[j+2] = 1
                    stop = 1
                    startLine = 1
                if (endp != 0 and wc[j] == endp_p1 and j != 0 and stop == 0):	
                    wc[j+1]   = endp_p1
                    wc[j]     = 0
                    newp[j+2] = 1
                    stop = 1
                if (endp != 0 and wc[j] >= endp_p1 and j != 0 and stop == 0):
                    wc[j+1]   = endp_p1
                    newp[j+2] = 1
                    stop = 1
                j -= 1
        for i in range(num_assets+1):
            if (wc[i] != 0):
                val = val | (1 << (num_assets - i))
                if (val == 0x81):
                    endLine = 1
                

														
								 
if __name__== "__main__":
    main()
