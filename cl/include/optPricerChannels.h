#include "optPricer.h"

#ifndef OPT_PRICER_KERNELS_CHANNELS_H
#define OPT_PRICER_KERNELS_CHANNELS_H


#define RETURN_BUFFER_DEPTH               512
#define MAX_EU_CHANNEL_0_PRICER_DEPTH   16384
#define MAX_EU_CHANNEL_1_PRICER_DEPTH       2
#define MAX_EU_CHANNEL_2_PRICER_DEPTH       1
#define MAX_EU_CHANNEL_3_PRICER_DEPTH       1
#define MAX_EU_CHANNEL_4_PRICER_DEPTH       1
#define MAX_EU_CHANNEL_5_PRICER_DEPTH       1
#define MAX_EU_CHANNEL_6_PRICER_DEPTH       1
#define MAX_EU_CHANNEL_7_PRICER_DEPTH       1
#define MAX_EU_CHANNEL_8_PRICER_DEPTH       1
#define MAX_EU_CHANNEL_9_PRICER_DEPTH       1
#define INT_BUFF_MAX_DEPTH             131072


// Channels to pass data from host
global	channel double  init_step_channel                     __attribute__((depth(NUM_ASSETS * NUM_ASSETS)));
global	channel double  strike_channel                        __attribute__((depth(NUM_ASSETS)));
global	channel double  init_step_next_channel[NUM_ASSETS]    __attribute__((depth(16)));
global	channel double  mult_coeff_channel[COMPUTE_DEPTH]     __attribute__((depth(NUM_BRANCHES)));
global	channel double  init_shares_channel                   __attribute__((depth(16)));
global	channel double  cash_channel                          __attribute__((depth(1)));
global	channel unsigned short max_min_channel                __attribute__((depth(1)));
global	channel unsigned short call_put_channel               __attribute__((depth(1)));
global	channel unsigned int tree_depth_channel               __attribute__((depth(1)));

// Channels to pass data to from Multi Tier Buffer
global	channel bool            writePayoutBufferFinishChannel[2]  __attribute__((depth(1)));
global	channel bool            readPayoutBufferFinishChannel[2]   __attribute__((depth(1)));
global	channel unsigned int    readPayoutBufferChannelActive[2]   __attribute__((depth(1)));
global	channel char            nextBuff                           __attribute__((depth(1)));
global  channel type_payout     writePayoutBufferChannel[2]        __attribute__((depth(256)));
global  channel type_payout     readPayoutBufferChannel[2]         __attribute__((depth(256)));
global  channel type_payout     multiBufferChannel2                __attribute__((depth(INT_BUFF_MAX_DEPTH)));

// Channels to pass pre computed Sorting order
global	channel unsigned short treePreSortStatusChannel                 __attribute__((depth(256)));

// Channels to pass Tree Depth Around
global	channel unsigned int currentTreeDepthChannel[COMPUTE_DEPTH+1] __attribute__((depth(1)));
global	channel unsigned short muxTreeDepthChannel                     __attribute__((depth(1)));

//StockPrice calculation Channels Out
global channel type_value   stockValueChannel                 __attribute__((depth(256)));

//First Payout calculation Channels Out
global channel type_payout  payoutChannel                     __attribute__((depth(256)));

//MUX Channels (2 in 1 out)
global channel type_payout  payoutChannelReturn  __attribute__((depth(RETURN_BUFFER_DEPTH)));
global channel type_payout  payoutChannelOut     __attribute__((depth(256)));
global channel bool         finishedChannel      __attribute__((depth(1)));


// Eu pricer Channels.
global channel type_payout  euPricerInChannel_0[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_0_PRICER_DEPTH)));
global channel type_payout  euPricerInChannel_1[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_1_PRICER_DEPTH)));
global channel type_payout  euPricerInChannel_2[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_2_PRICER_DEPTH)));
global channel type_payout  euPricerInChannel_3[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_3_PRICER_DEPTH)));
global channel type_payout  euPricerInChannel_4[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_4_PRICER_DEPTH)));
global channel type_payout  euPricerInChannel_5[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_5_PRICER_DEPTH)));
global channel type_payout  euPricerInChannel_6[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_6_PRICER_DEPTH)));
global channel type_payout  euPricerInChannel_7[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_7_PRICER_DEPTH)));
global channel type_payout  euPricerInChannel_8[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_8_PRICER_DEPTH)));
global channel type_payout  euPricerInChannel_9[COMPUTE_DEPTH]  __attribute__((depth(MAX_EU_CHANNEL_9_PRICER_DEPTH)));

global channel type_payout  euPricerOutChannel[COMPUTE_DEPTH+1] __attribute__((depth(256)));
global channel type_payout  finalOptionPrice[COMPUTE_DEPTH]     __attribute__((depth(1)));



#endif

