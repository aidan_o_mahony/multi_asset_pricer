#ifndef OPT_PRICER_KERNELS_H
#define OPT_PRICER_KERNELS_H

#define MAX_NUM_ASSETS                  9
#define NUM_ASSETS                      2
#define NUM_BRANCHES                    3
#define INT_MEM_TREE_DEPTH            128
#define MAX_TREE_DEPTH               2048
#define COMPUTE_DEPTH                  50
	
#define READ_POINTER_MASK             0x3
#define WRITE_POINTER_MASK            0x3

#define TREE_START_LINE_BIT 		       19
#define TREE_END_LINE_BIT   		       18
#define TREE_FINISHED_BIT   		       17
#define TREE_LAST_BIT       		       16
#define TREE_DEPTH_MASK     		    0X7FFF
#define END_DEPTH_COUNTER_BIT          15
#define DEPTH_COUNTER_MASK         0x1FFF

#define LAST_OFFSET                    20

#define TREE_PRESORT_START_LINE_BIT    15
#define TREE_PRESORT_END_LINE_BIT      14
#define TREE_PRESORT_FIRST_LINE_BIT    13
#define TREE_PRESORT_ORDER_MASK     0XFFF


#define INT_BUFF_DEPTH_COUNTER_BIT     12
#define INT_BUFF_MAX_TREE_DEPTH       511 
	
#define INT_BUFF_TREE_DEPTH_MASK    0XFFF

#define DDR_BUFF_DEPTH           33554432
#define PAYOUT_N                       16


typedef struct type_stockPriceTree {  double          stockPrice[NUM_ASSETS]; 
				      unsigned short numberOfBranches;
				      unsigned short currentBranch;
				      unsigned short returnBranch;
				      unsigned short index;
				} type_stockPriceTree;

typedef struct type_value   	{	double          value[NUM_ASSETS]; 
					bool           finished;
				} type_value;

typedef struct type_payout         {	
	        double       payout; 
					unsigned int treeStatus;
					unsigned int treePreSortStatus;
				} type_payout;




#endif

