#include "include/optPricer.h"

#pragma OPENCL EXTENSION cl_intel_channels : enable
//__attribute__((max_global_work_dim(0)))
//__attribute__((autorun))

kernel void returnBuffer ( __global __attribute__((buffer_location("DDR"))) type_payout * restrict payoutBuffer0,
			                     __global __attribute__((buffer_location("DDR"))) type_payout * restrict payoutBuffer1 )
{	
  type_payout x, y;

	unsigned short endLine;
	unsigned short startLine;
	
	
	unsigned short treeDepth;
	unsigned int    __attribute__((register)) readPointer[3]  = { 0, 0, 0};
	unsigned int    __attribute__((register)) writePointer[3] = { 0, 0, 0};
	unsigned int intBuffContent  = 0;
	bool finished = 0;
	unsigned char activeWriteBuffer = 0;
	unsigned char activeReadBuffer  = 3;
	bool status;
	type_payout payoutBuffer2[INT_BUFF_MAX_DEPTH];
	//printf ("Start\n");  
	// DISABLE LOOP PIPELINING.  We only read or write one buffer at a time This will only work with 19.2
	#pragma disable_loop_pipelining
	while (!finished) {
		/* Reading from previous Channel and Writing to internal/external buffers */
		x = read_channel_nb_intel(euPricerOutChannel[COMPUTE_DEPTH], &status);
		if (status) {
			startLine = (x.treeStatus >> TREE_START_LINE_BIT) & 0x0001;
			endLine   = (x.treeStatus >> TREE_END_LINE_BIT)   & 0x0001;
			treeDepth = (x.treeStatus) & TREE_DEPTH_MASK;
			
			if (activeWriteBuffer == 0) {
				if (startLine){
					writePointer[0]  = 0;
					//printf ("StartLIne, AWB: %d, WP[0]: %d, WP[1]: %d, WP[2]: %d, : %d, %d\n",  activeWriteBuffer,writePointer[0],writePointer[1],writePointer[2],endLine, startLine);  
				}	
				payoutBuffer0[writePointer[0]] = x;
				++writePointer[0];
				if (endLine){
					activeReadBuffer = 0;
					readPointer[0]   = 0;
					//printf ("EndLIne ARB: %d, RP[0]: %d, RP[1]: %d, RP[2]: %d, : %d, %d\n",  activeReadBuffer, readPointer[0], readPointer[1], readPointer[2], endLine, startLine);  
				}
			} else if (activeWriteBuffer == 1) {
				if (startLine){
					writePointer[1]  = 0;
					//printf ("StartLIne AWB: %d, WP[0]: %d, WP[1]: %d, WP[2]: %d, : %d, %d\n",  activeWriteBuffer, writePointer[0], writePointer[1], writePointer[2], endLine, startLine);  
				}	
				payoutBuffer1[writePointer[1]] = x;
				++writePointer[1];
				if (endLine){
					activeReadBuffer = 1;
					readPointer[1]   = 0;
					//printf ("EndLIne ARB: %d, RP[0]: %d, RP[1]: %d, RP[2]: %d, : %d, %d\n",  activeReadBuffer, readPointer[0], readPointer[1], readPointer[2], endLine, startLine);  
				}
		  	} else if (activeWriteBuffer == 2) {
					payoutBuffer2[writePointer[2]] = x;
					writePointer[2] =	(writePointer[2] + 1) & INT_BUFF_MAX_DEPTH_MASK; // Buffer 2 is a circular Buffer
					++intBuffContent;
					if (endLine){
						activeReadBuffer = 2;
						//printf ("EndLIne AWB: %d, WP[2]: %d, RD[2]: %d, TreeDepth: %d, %d, %d\n",  activeReadBuffer, writePointer[2], readPointer[2], endLine, startLine, treeDepth);  
					}
		  	}  	
			if (endLine) {
				if (activeWriteBuffer != 2) {
					if (activeWriteBuffer == 0) {
						if (writePointer[0] <= INT_BUFF_MAX_DEPTH) activeWriteBuffer = 2;
						else                                       activeWriteBuffer = 1;
					} else {
						if (writePointer[1] <= INT_BUFF_MAX_DEPTH) activeWriteBuffer = 2;
						else                                       activeWriteBuffer = 0;
					}
					//printf ("EndLIne AWB: %d, WP[0]: %d, WP[1]: %d, : %d, %d, %d\n",  activeWriteBuffer, writePointer[0], writePointer[1], endLine, startLine, treeDepth);  
				}
	  		}
		}		
		/* End of Writting to Internal/External Buffers */
		
		
		/* Reading from Internal/External Buffers and writing to nect Channel */
		bool writeChannel = 0;
		if (activeReadBuffer == 0) {
			y = payoutBuffer0[readPointer[0]];
			++readPointer[0];
			writeChannel = 1;
			//if (y.endLine) {
			if ((y.treeStatus >> TREE_END_LINE_BIT) & 0x0001) {
				activeReadBuffer = 3;
			}
		} else if (activeReadBuffer == 1) {
			y = payoutBuffer1[readPointer[1]];
			++readPointer[1];
			writeChannel = 1;
			if ((y.treeStatus >> TREE_END_LINE_BIT) & 0x0001) {
				activeReadBuffer = 3;
			}
		} else if (activeReadBuffer == 2) {
			if (intBuffContent != 0 ) {
				y = payoutBuffer2[readPointer[2]];
				readPointer[2] = (readPointer[2] + 1) & INT_BUFF_MAX_DEPTH_MASK; // Buffer 2 is a circular Buffer.
				--intBuffContent;
				writeChannel = 1;
			}
		}
		
		if (writeChannel) {
			write_channel_intel(payoutChannelReturn, y);
		}
		//if (y.treeDepth == 0 && y.endLine == 1) finished = 1;
		if ((y.treeStatus & TREE_DEPTH_MASK == 0) && ((y.treeStatus >> TREE_END_LINE_BIT) & 0x0001) == 1) finished = 1;
		/* End of Reading from Internal/External Buffers */
	}
	//printf("EXIT BUFFER\n");
}
