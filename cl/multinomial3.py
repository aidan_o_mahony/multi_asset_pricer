def main():
    """ Code that takes change as input
    and returns the change
    """
    import numpy as np
    treeDepth = int(input("Tree Depth: "))
    print ("hello")
    num_assets      = 3
    num_branches    = num_assets + 1
    num_branches_m2 = num_assets - 1
    

    
    fifoMarker       = np.array([0,0,0,0],dtype=np.int32)
    pastFifoMarker   = np.array([[0,0,0,0,0],
                                [0,0,0,0,0],
                                [0,0,0,0,0],
                                [0,0,0,0,0]],dtype=np.int32)

    numberOfBranches = np.array([0,0,0,0],dtype=np.int32)
    currentBranch    = np.array([1,0,0,0],dtype=np.int32)
    returnBranch     = np.array([1,0,0,0],dtype=np.int32)
    index            = np.array([0,0,0,0],dtype=np.int32)
    finished             = 0
    currentIndex         = 0
    nextIndex            = 0
    moveSideways         = 0
    fifoMarker[0]        = treeDepth+1
    pastFifoMarker[0][0] = treeDepth+1
    numberOfBranches[0]  = treeDepth
    val = 0x8
    endLine = 0
    startLine = 1
    print (fifoMarker, format(val, '08b'), startLine, endLine)
    while (1):
        if (moveSideways):
            fifoMarker[currentIndex]   -=1
            fifoMarker[currentIndex+1] +=1
        else:
            pastFifoMarker[currentIndex][currentIndex]   = pastFifoMarker[currentIndex][currentIndex] - 1            
            pastFifoMarker[currentIndex][currentIndex+1] = pastFifoMarker[currentIndex][currentIndex+1] + 1
            for i in range(num_branches):
                pastFifoMarker[currentIndex+1][i] = pastFifoMarker[currentIndex][i]
                fifoMarker[i] = pastFifoMarker[currentIndex][i]
            pastFifoMarker[currentIndex+1][currentIndex+1] = pastFifoMarker[currentIndex+1][currentIndex+1] - 1            
            pastFifoMarker[currentIndex+1][currentIndex+2] = pastFifoMarker[currentIndex+1][currentIndex+2] + 1
        "print (pastFifoMarker, fifoMarker, moveSideways, currentIndex, nextIndex, currentBranch, returnBranch)"
        val = 0
        startLine = 0
        if (endLine == 1):
            startLine = 1
        endLine = 0
        for i in range(num_assets+1):
            if (fifoMarker[i] != 0):
                val = val | (1 << (num_assets - i))
                if (val == 0x9):
                    endLine = 1
        print (fifoMarker, format(val, '08b'), startLine, endLine)
        if finished == 1:
            break
        moveSideways = 1
        nextIndex = currentIndex + 1
        "print (moveSideways, num_branches_m2, currentIndex, nextIndex, currentBranch, returnBranch)"
        if (currentIndex < num_branches_m2):
            numberOfBranches[nextIndex] = currentBranch[currentIndex]
            currentBranch[nextIndex]    = 1
            index[nextIndex]            = currentBranch[currentIndex] + 1
            if ((numberOfBranches[nextIndex] == 1) or (nextIndex == num_branches_m2) or (numberOfBranches[nextIndex] == currentBranch[nextIndex])):
                returnBranch[nextIndex] = returnBranch[currentIndex]
            else:
                returnBranch[nextIndex] = nextIndex + 1
            currentIndex = nextIndex
        elif (numberOfBranches[currentIndex] != currentBranch[currentIndex]):
            currentBranch[currentIndex] +=1
            moveSideways = 0
            if (( currentBranch[currentIndex] == treeDepth) and (numberOfBranches[currentIndex] == treeDepth)) :
                finished = 1
        else:
            moveSideways = 0
            if (currentBranch[currentIndex] == (treeDepth)):
                finished = 1
            else:
                currentIndex = returnBranch[currentIndex] - 1
                currentBranch[currentIndex] +=1
                if (currentIndex > 0 and ( numberOfBranches[currentIndex] == currentBranch[currentIndex] )):
                    returnBranch[currentIndex] = returnBranch[currentIndex - 1]

if __name__== "__main__":
    main()

