#include "include/optPricer.h"


#pragma OPENCL EXTENSION cl_intel_channels : enable
kernel void dataPreSorter ( unsigned short treeDepth)
{	

	unsigned short __attribute__((register)) fifoMarker[NUM_BRANCHES];
	unsigned short __attribute__((register)) pastFifoMarker[NUM_BRANCHES][NUM_ASSETS+2];
	unsigned short __attribute__((register)) numberOfBranches[NUM_BRANCHES];
	unsigned short __attribute__((register)) currentBranch[NUM_BRANCHES];
	unsigned short __attribute__((register)) returnBranch[NUM_BRANCHES];
	unsigned short __attribute__((register)) index[NUM_BRANCHES];



	unsigned char endLine       = 0;
	unsigned char startLine     = 1;
  unsigned char finished      = 0;
  unsigned short currentIndex  = 0;
  unsigned short nextIndex     = 0;
  unsigned char  moveSideways  = 0;
  
  // Initialize Arrays
  #pragma unroll NUM_BRANCHES
  for (char i = 0; i < NUM_BRANCHES ; i++) {
  	fifoMarker[i] = 0;
  	numberOfBranches[i] = 0;
  	currentBranch[i] = 0;
  	returnBranch[i] = 0;
  	index[i] = 0;
  }
  #pragma unroll NUM_BRANCHES
  for (char i = 0; i < NUM_BRANCHES ; i++) {
  	#pragma unroll NUM_ASSETS+2
  	for (char j = 0; j < NUM_ASSETS+2 ; j++) {
  		pastFifoMarker[i][j] = 0;
  	}
  }
  fifoMarker[0]        = treeDepth+1;
  pastFifoMarker[0][0] = treeDepth+1;
  numberOfBranches[0]  = treeDepth;
  currentBranch[0]     = 1;
  returnBranch[0]      = 1;
  

  unsigned short treePreSortStatus = 0x0001 | (1 << TREE_PRESORT_START_LINE_BIT) | (1 << TREE_PRESORT_FIRST_LINE_BIT);
	//printf ("startLine: %d, endLine:%d, treePreSortStatus: %x\n",startLine, endLine, treePreSortStatus);
	write_channel_intel(treePreSortStatusChannel, treePreSortStatus);


	
	#pragma ii 10
	while (1) {
	  if (moveSideways) {
	      --fifoMarker[currentIndex];
	      ++fifoMarker[currentIndex+1];
	  } else {
	      pastFifoMarker[currentIndex][currentIndex]   = pastFifoMarker[currentIndex][currentIndex] - 1;            
	      pastFifoMarker[currentIndex][currentIndex+1] = pastFifoMarker[currentIndex][currentIndex+1] + 1;
	      #pragma unroll NUM_BRANCHES
	      for (char i = 0; i < NUM_BRANCHES; i++){
	          pastFifoMarker[currentIndex+1][i] = pastFifoMarker[currentIndex][i];
	          fifoMarker[i] = pastFifoMarker[currentIndex][i];
	      }
	      pastFifoMarker[currentIndex+1][currentIndex+1] = pastFifoMarker[currentIndex+1][currentIndex+1] - 1;            
	      pastFifoMarker[currentIndex+1][currentIndex+2] = pastFifoMarker[currentIndex+1][currentIndex+2] + 1;
	  }
	  // Start Line and end Line delimiters

	  //startLine = endLine;
	  // startLine always starts one after the endLine.
	  treePreSortStatus = 0;
	  treePreSortStatus = ((endLine & 0x01) << TREE_PRESORT_START_LINE_BIT);
	  endLine = 0;

		#pragma unroll NUM_BRANCHES
		for (char i = 0; i < NUM_BRANCHES; i++) {
			if (fifoMarker[i] != 0) {
				treePreSortStatus |= (1 << i);
			}
		}
		if ((treePreSortStatus & TREE_PRESORT_ORDER_MASK) == ((1 << NUM_ASSETS) | 0x01)) {
			endLine = 1;
			treePreSortStatus |= (1 << TREE_PRESORT_END_LINE_BIT);
		}
		//printf ("startLine: %d, endLine:%d, treePreSortStatus: %x\n",startLine, endLine, treePreSortStatus);
		write_channel_intel(treePreSortStatusChannel, treePreSortStatus);
		
    if (finished == 1) {
        break;
    }
    moveSideways = 1;
    nextIndex = currentIndex + 1;
    if (currentIndex < (NUM_BRANCHES -2)) {
        numberOfBranches[nextIndex] = currentBranch[currentIndex];
        currentBranch[nextIndex]    = 1;
        index[nextIndex]            = currentBranch[currentIndex] + 1;
        if ((numberOfBranches[nextIndex] == 1) || (nextIndex == (NUM_BRANCHES -2)) || (numberOfBranches[nextIndex] == currentBranch[nextIndex])) {
            returnBranch[nextIndex] = returnBranch[currentIndex];
        } else {
            returnBranch[nextIndex] = nextIndex + 1;
        }
        currentIndex = nextIndex;
    } else if (numberOfBranches[currentIndex] != currentBranch[currentIndex]) {
        ++currentBranch[currentIndex];
        moveSideways = 0;
        if (( currentBranch[currentIndex] == treeDepth) && (numberOfBranches[currentIndex] == treeDepth)) {
            finished = 1;
        }
    } else {
        moveSideways = 0;
        if (currentBranch[currentIndex] == (treeDepth)) {
            finished = 1;
        } else {
            currentIndex = returnBranch[currentIndex] - 1;
            ++currentBranch[currentIndex];
            if (currentIndex > 0 && ( numberOfBranches[currentIndex] == currentBranch[currentIndex] )) {
                returnBranch[currentIndex] = returnBranch[currentIndex - 1];
            }
        }
    }
	}
	//printf ("EXIT data PRE Sorter\n");
}
