#include "inverse.h"
void matrix_inv(float_type **a,ushort k, float_type **inverse) 
{
  float_type d;
  int i,j;
  d=determinant(a,k);
  if (d==0)
   printf("\n Matrix is not full rank\n");
  else
    cofactor(a,k,inverse);
}
 
/*For calculating Determinant of the Matrix */
float_type determinant(float_type **a,ushort k)
{
  float_type s=1.0,det=0.0;
  float_type **b;
  int i,j,m,n,c;

  b=(float_type **)malloc(k*sizeof(float_type *));
  assert_mem(b);
  for (i=0;i<k;i++){
  b[i]=(float_type *)malloc(k*sizeof(float_type ));
  assert_mem(b[i]);
  }

  if (k==1)
    {
     return (a[0][0]);
    }
  else
    {
     det=0;
     for (c=0;c<k;c++)
       {
        m=0;
        n=0;
        for (i=0;i<k;i++)
          {
            for (j=0;j<k;j++)
              {
                b[i][j]=0;
                if (i != 0 && j != c)
                 {
                   b[m][n]=a[i][j];
                   if (n<(k-2))
                    n++;
                   else
                    {
                     n=0;
                     m++;
                     }
                   }
               }
             }
          det=det + s * (a[0][c] * determinant(b,k-1));
          s=-1 * s;
          }
    }
  for (i=0;i<k;i++){
  free(b[i]);
  }
  free(b);
 
    return (det);
}
 
void cofactor(float_type **num,ushort f, float_type **inverse)
{
 float_type **b,**fac;
 int p,q,m,n,i,j;

  b=(float_type **)malloc(f*sizeof(float_type *));
  assert_mem(b);
  for (i=0;i<f;i++){
  b[i]=(float_type *)malloc(f*sizeof(float_type ));
  assert_mem(b[i]);
  }
  fac=(float_type **)malloc(f*sizeof(float_type *));
  assert_mem(fac);
  for (i=0;i<f;i++){
  fac[i]=(float_type *)malloc(f*sizeof(float_type ));
  assert_mem(fac[i]);
  }


 for (q=0;q<f;q++)
 {
   for (p=0;p<f;p++)
    {
     m=0;
     n=0;
     for (i=0;i<f;i++)
     {
       for (j=0;j<f;j++)
        {
          if (i != q && j != p)
          {
            b[m][n]=num[i][j];
            if (n<(f-2))
             n++;
            else
             {
               n=0;
               m++;
               }
            }
        }
      }
      fac[q][p]=POW_FUNC(-1,q + p) * determinant(b,f-1);
    }
  }
 transpose(num,fac,f,inverse);

  for (i=0;i<f;i++){
  free(b[i]);
  free(fac[i]);
  }
  free(b);
  free(fac);
}
/*Finding transpose of matrix*/ 
void transpose(float_type **num,float_type **fac,ushort r,float_type **inverse)
{
  int i,j;
  float_type  **b;
  float_type d;
  b=(float_type **)malloc(r*sizeof(float_type *));
  assert_mem(b);
  for (i=0;i<r;i++){
  b[i]=(float_type *)malloc(r*sizeof(float_type ));
  assert_mem(b[i]);
  }

 
  for (i=0;i<r;i++)
    {
     for (j=0;j<r;j++)
       {
         b[i][j]=fac[j][i];
        }
    }
  d=determinant(num,r);
  for (i=0;i<r;i++)
    {
     for (j=0;j<r;j++)
       {
        inverse[i][j]=b[i][j] / d;
        }
    }
  for (i=0;i<r;i++){
  free(b[i]);
  }
  free(b);

}

