void usage_opt_pricer();
void assert_mem(void *ptr_to_new_mem);
void print_shares(float_type *shares,ushort num_dim, char *heading);
void print_partition(ushort *partition,ushort num_dim, char *heading);
float_type max_array(float_type *array,ushort dim);
float_type min_array(float_type *array,ushort dim);
const char *byte_to_binary(int x);
void read_config(FILE *fid,config_set *conf_set, char *config_file);
void  prinf_float_type(char *head,float_type val,ushort frac_digits);
void  fprintf_float_type(FILE *fid,char *head,float_type val,ushort frac_digits);
void prinf_float_type_hex(char *head,float_type val);
void print_gsl_matrix(gsl_matrix *m);
void print_gsl_vector(gsl_vector *v);
void print_float_type_matrix(float_type  **m,const ulong row,const  ulong col, char *head);
void print_float_type_vector(float_type *v,ulong dim,char * head);
void convert_to_gls_mat(float_type **m,ulong rows,ulong cols,gsl_matrix *m_gsl);

void top_tb_gen(config_set *conf_set,ushort num_dim,float_type *init_share_d0,float_type **share_mult_vect, opt_price_params *opp);
void init_tb_gen(config_set *conf_set,ushort num_dim,float_type *init_share_d0,float_type **share_mult_vect,generic_fifo *fifo_partis,ushort *init_partition,generic_fifo **fifo_value_vect,generic_fifo *fifo_shares);

