#include<stdio.h>
#include<math.h>
//#include<conio.h>
 #include "opt_pricer.h"

float_type determinant(float_type **a, ushort k);
void cofactor(float_type **num,ushort f, float_type **inverse);
void transpose(float_type **num,float_type **fac,ushort r, float_type **inverse);
void matrix_inv(float_type **a,ushort k, float_type **inverse) ;
